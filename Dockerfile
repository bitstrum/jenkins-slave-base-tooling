FROM debian:10

RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
    git \
    ssh \
 && apt-get clean && rm -rf /var/lib/apt/lists/*
